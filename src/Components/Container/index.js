import React, {Component} from 'react'
import ListItem from '../Presentational/index';
import './style.css';
export default class List extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            clickedId: null
        }

    }
    handleClick = (id) => {

        this.setState({clickedId:id})

    }

    render() {
        return (
            <div>
                {
                    this
                        .props
                        .items
                        .map(item => {
                            return (
                                <ListItem
                                    title={item.title}
                                    key={item.id}
                                    id={item.id}
                                    description={item.children}
                                    isShowed={this.state.clickedId === item.id}
                                    onClick={this.handleClick}/>
)
                        })
                }
            </div>
        )
    }
}
