import React from 'react'
import './style.css';

function DescriptionItem(props) {
  return <li>{props.title}</li>  
}

export default class ListItem extends React.Component {
    constructor(props) {
        super(props);

    }
    handleClick = () => {
        this
            .props
            .onClick(this.props.id)
    }
    render() {
        return (
            <div className='container'>
                <h2 onClick={this.handleClick}>

                    {this.props.title}
                    {
                        this.props.isShowed
                            ? <i className="fa fa-sort-up icon"></i>
                            : <i className="fa fa-sort-down icon"></i>
                    }

                    <br/>

                </h2>
                <ul className='list'>
               {this.props.isShowed &&this.props.description.map(item=>{
                   return <DescriptionItem title={item.description}/>
               })}
                </ul>
            </div>
        )
    }
}
