import React from 'react';
import logo from './logo.svg';
import './App.css';
import List from './Components/Container/index'
const list = [
    {
        id: '1',
        title: 'feautures',
        children: [
            {
                id:'1',
                description: 'Edition Dealer Only'
            }, {
                id:'2',
                description: 'Use Everyday',
            }
            ,{
                id:'3',
                description:'Opening Action Manual dual KVT ball bearings'
            }
        ]
    }, {
        id: '2',
        title: 'specifications',
        children: [
            {
               id:'1',
                description: 'Blade Length 4.6 in. (11.7 cm)'
            }, {
               id:'2',
                description: 'Blade Steel Sandvik 14C28N'
            }
        ]
    }, {
        id: '3',
        title: 'more',
        children: [
            {
               id:'1',
                description: 'Year Released :2020'
            },
        ]
    }
]
function App() {
    return (<List items={list}/>);
}

export default App;
